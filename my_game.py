import pygame
import libs.screencontroller

SCREEN_WIDTH = 900 * 2
SCREEN_HEIGHT = 468 * 2

pygame.init()

sc = libs.screencontroller.ScreenController(SCREEN_WIDTH, SCREEN_HEIGHT)
sc.run()

pygame.quit()
