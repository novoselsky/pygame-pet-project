import sys
import pygame
try:
	sys.path.append('../')
	from pygamepetproject.libs.thing import Thing, Character, Hero
except ModuleNotFoundError as e:
	from libs.thing import Thing, Character, Hero

test_location = [10, 10]
test_size = [20, 20]
try:
	test_sprite = pygame.image.load("assets/adventurer-air-attack1-00.png")
except FileNotFoundError as e:
	test_sprite = pygame.image.load("../assets/adventurer-air-attack1-00.png")


DEFAULT_HERO_PROPERTIES = {
	'location' : (0, 0),
	'size' : (0, 0),
	'sprite' : test_sprite,
	'is_visible' : True,
	'permeable' : False,
	'speed' : 0,
	'acceleration' : 0,
	'max_hp' : 3,
	'hp' : 3,
	'score' : 0,
	'controlled' : True,
}



properties = {
	'location' : test_location,
	'size' : test_size,
	'sprite' : test_sprite,
	'is_visible' : True,
	'permeable' : True,
}


test_thing = Thing(location = test_location,size = test_size,sprite = test_sprite,is_visible = True,permeable = True)
test_thing = Thing(**DEFAULT_HERO_PROPERTIES)
print("Thing is OK")

char_props = properties
char_props['speed'] = (10, 10)
char_props['acceleration'] = (20, 20)

test_character = Character(location = test_location,size = test_size,sprite = test_sprite,is_visible = True,permeable = True, speed = (10, 10), acceleration = (20, 20))
test_character = Character(**char_props)
print("Character is OK")

hero_props = char_props
hero_props['max_hp'] = 3
hero_props['hp'] = 3
hero_props['score'] = 0

test_hero = Hero(**DEFAULT_HERO_PROPERTIES)
print("Hero is OK")
