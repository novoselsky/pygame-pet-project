from libs.movement_handler import State, StateHandler
import pygame

LEFT_KEY = pygame.K_LEFT
RIGHT_KEY = pygame.K_RIGHT
UP_KEY = pygame.K_UP
SPACE_KEY = pygame.K_SPACE


def none_def(state_handler):
    pass


EXAMPLE_OF_STATE_PROPERTIES = {
    "name": "Empty_name",
    "change_conditions_defs_list": list(),
    "start_def": none_def,
    "update_def": none_def,
    "end_def": none_def,
    "keys_press_dictionary": dict(),
    "keys_release_dictionary": dict(),
}


# STANDARD CONDITIONS
def should_fall(data):
    hitboxes = data["hitboxes"]
    legs = hitboxes["legs"]
    platforms = data["platforms_group"]
    if legs.collidelist(platforms) == -1:
        return "FALL"


def should_stand(data):
    hitboxes = data["hitboxes"]
    legs = hitboxes["legs"]
    platforms = data["platforms_group"]
    if legs.collidelist(platforms) != -1:
        return "STAND"


def get_forcing_next_state_def(next_state):
    def forcing_def(state_handler):
        state_handler.forced_next_state = next_state

    return forcing_def


def stop_moving_in_x_axis(state_handler: StateHandler):
    state_handler.movement_handler.speed[0] = 0


# STANDING
def stand(state_handler: StateHandler):
    if state_handler.movement_handler.is_moving_right:  # moving right
        state_handler.forced_next_state = "RUN_RIGHT"
    elif state_handler.movement_handler.is_moving_left:  # moving left
        state_handler.forced_next_state = "RUN_LEFT"
    state_handler.movement_handler.acceleration[0] = 0
    state_handler.movement_handler.speed[0] = 0
    state_handler.movement_handler.acceleration[1] = 0
    state_handler.movement_handler.speed[1] = 0
    state_handler.animation_handler.force_animation("default")


STAND_state_properties = {
    "name": "STAND",
    "change_conditions_defs_list": [should_fall],
    "start_def": stand,
    "update_def": none_def,
    "end_def": none_def,
    "keys_press_dictionary": {
        RIGHT_KEY: get_forcing_next_state_def("RUN_RIGHT"),
        LEFT_KEY: get_forcing_next_state_def("RUN_LEFT"),
        SPACE_KEY: get_forcing_next_state_def("JUMP"),
    },
    "keys_release_dictionary": dict(),
}

STAND_state = State(**STAND_state_properties)

# RUNNING
SPEED = 2


# RIGHT
def moving_right(state_handler: StateHandler):
    state_handler.movement_handler.speed[0] = SPEED
    state_handler.animation_handler.set_flip(False)
    state_handler.animation_handler.force_animation("running")


RUN_RIGHT_state_properties = {
    "name": "RUN_RIGHT",
    "change_conditions_defs_list": [should_fall],
    "start_def": moving_right,
    "update_def": none_def,
    "end_def": none_def,
    "keys_press_dictionary": {
        LEFT_KEY: get_forcing_next_state_def("RUN_LEFT"),
        SPACE_KEY: get_forcing_next_state_def("JUMP"),
    },
    "keys_release_dictionary": {
        RIGHT_KEY: get_forcing_next_state_def("STAND"),
    },
}

RUN_RIGHT_state = State(**RUN_RIGHT_state_properties)


# LEFT
def moving_left(state_handler: StateHandler):
    state_handler.movement_handler.speed[0] = -SPEED
    state_handler.animation_handler.set_flip(True)
    state_handler.animation_handler.force_animation("running")


RUN_LEFT_state_properties = {
    "name": "RUN_LEFT",
    "change_conditions_defs_list": [should_fall],
    "start_def": moving_left,
    "update_def": none_def,
    "end_def": none_def,
    "keys_press_dictionary": {
        RIGHT_KEY: get_forcing_next_state_def("RUN_RIGHT"),
        SPACE_KEY: get_forcing_next_state_def("JUMP"),
    },
    "keys_release_dictionary": {
        LEFT_KEY: get_forcing_next_state_def("STAND"),
    },
}

RUN_LEFT_state = State(**RUN_LEFT_state_properties)


# JUMPING
def jump(state_handler: StateHandler):
    state_handler.animation_handler.force_animation("jumping")
    state_handler.movement_handler.speed[1] = -8
    force_fall_def = get_forcing_next_state_def("FALL")
    force_fall_def(state_handler)


JUMP_state_properties = {
    "name": "JUMP",
    "change_conditions_defs_list": [should_fall],
    "start_def": jump,
    "update_def": none_def,
    "end_def": none_def,
    "keys_press_dictionary": dict(),
    "keys_release_dictionary": dict(),
}

JUMP_state = State(**JUMP_state_properties)


# FALLING
def fall(state_handler: StateHandler):
    state_handler.movement_handler.acceleration[1] = 0.1
    state_handler.animation_handler.add_animation_to_queue("falling")


def stop_moving_right_if_needed(state_handler: StateHandler):
    if state_handler.movement_handler.speed[0] > 0:
        stop_moving_in_x_axis(state_handler)


def stop_moving_left_if_needed(state_handler: StateHandler):
    if state_handler.movement_handler.speed[0] < 0:
        stop_moving_in_x_axis(state_handler)


def moving_right_without_animation(state_handler: StateHandler):
    state_handler.movement_handler.speed[0] = SPEED
    state_handler.animation_handler.set_flip(False)


def moving_left_without_animation(state_handler: StateHandler):
    state_handler.movement_handler.speed[0] = -SPEED
    state_handler.animation_handler.set_flip(True)


# FALL STRAIGHT DOWN
FALL_state_properties = {
    "name": "FALL",
    "change_conditions_defs_list": [should_stand],
    "start_def": fall,
    "update_def": none_def,
    "end_def": none_def,
    "keys_press_dictionary": {
        RIGHT_KEY: moving_right_without_animation,
        LEFT_KEY: moving_left_without_animation,
    },
    "keys_release_dictionary": {
        RIGHT_KEY: stop_moving_right_if_needed,
        LEFT_KEY: stop_moving_left_if_needed,
    },
}
FALL_state = State(**FALL_state_properties)

STATES_DICT = {
    "STAND": STAND_state,
    "RUN_RIGHT": RUN_RIGHT_state,
    "RUN_LEFT": RUN_LEFT_state,
    "FALL": FALL_state,
    "JUMP": JUMP_state,
}
