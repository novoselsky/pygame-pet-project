import pygame
import logging
from libs.animationhandler import AnimationHandler

LEFT_KEY = pygame.K_LEFT
RIGHT_KEY = pygame.K_RIGHT
UP_KEY = pygame.K_UP
SPACE_KEY = pygame.K_SPACE


class State:
    def __init__(self, **properties):
        self.name = properties["name"]
        self.change_conditions_defs_list = properties["change_conditions_defs_list"]
        self.keys_press_dictionary = properties["keys_press_dictionary"]
        self.keys_release_dictionary = properties["keys_release_dictionary"]
        self.start_def = properties["start_def"]
        self.update_def = properties["update_def"]
        self.end_def = properties["end_def"]

    def get_key_press_def(self, key):
        if key in self.keys_press_dictionary:
            return self.keys_press_dictionary[key]

    def get_key_release_def(self, key):
        if key in self.keys_release_dictionary:
            return self.keys_release_dictionary[key]


class MovementHandler:
    def __init__(self, hitboxes):
        self.speed = pygame.math.Vector2(0, 0)
        self.acceleration = pygame.math.Vector2()
        self.hitboxes = hitboxes
        self.is_moving_right = False
        self.is_moving_left = False

    def start_moving_right(self):
        self.is_moving_right = True
        self.is_moving_left = False

    def start_moving_left(self):
        self.is_moving_right = False
        self.is_moving_left = True

    def stop_moving_right(self):
        self.is_moving_right = False

    def stop_moving_left(self):
        self.is_moving_left = False

    def stop_moving(self):
        self.is_moving_right = False
        self.is_moving_left = False

    def update_location(self):
        body = self.hitboxes["body"]
        head = self.hitboxes["head"]
        legs = self.hitboxes["legs"]
        body.topleft += self.speed
        self.speed += self.acceleration
        head.topleft = body.topleft
        legs.bottomleft = body.bottomleft


class StateHandler:
    animation_handler: AnimationHandler
    movement_handler: MovementHandler
    current_state: State

    def __init__(self, level_data, hero):
        self.level_data = level_data
        self.states_dict = hero.states_dict
        self.movement_handler = hero.movement_handler
        self.animation_handler = hero.animation_handler
        self.current_state = self.states_dict["STAND"]
        self.forced_next_state = None

    def __run_start_def(self):
        self.current_state.start_def(self)

    def __run_update_def(self):
        self.current_state.update_def(self)

    def __run_end_def(self):
        self.current_state.end_def(self)

    def __change_state(self, next_state_key):
        self.__run_end_def()
        self.current_state = self.states_dict[next_state_key]
        self.__run_start_def()

    def __change_state_if_possible(self, next_state_key):
        if next_state_key in self.states_dict:
            self.__change_state(next_state_key)
        else:
            logging.warning(f"KEY ERROR WHILE CHANGING STATE {self.current_state.name}\n"
                            f"NEXT STATE KEY WAS {next_state_key}")

    def __get_next_state_key_if_exist(self):
        if self.forced_next_state:
            next_state = self.forced_next_state
            self.forced_next_state = None
            return next_state
        for condition in self.current_state.change_conditions_defs_list:
            next_state = condition(self.level_data)
            if next_state:
                return next_state
        return None  # if next state doesn't exist

    def __change_state_if_needed(self):
        next_state_key = self.__get_next_state_key_if_exist()
        if next_state_key:
            self.__change_state_if_possible(next_state_key)

    def handle_key_press(self, key):
        if key == RIGHT_KEY:  # TODO Change movement handler moving functions
            self.movement_handler.start_moving_right()
        elif key == LEFT_KEY:
            self.movement_handler.start_moving_left()
        key_press_def = self.current_state.get_key_press_def(key)
        if key_press_def:
            key_press_def(self)

    def handle_key_release(self, key):
        if key == RIGHT_KEY:
            self.movement_handler.stop_moving_right()
        elif key == LEFT_KEY:
            self.movement_handler.stop_moving_left()
        key_release_def = self.current_state.get_key_release_def(key)
        if key_release_def:
            key_release_def(self)

    def handle(self):
        self.__change_state_if_needed()
        self.__run_update_def()
