import pygame
import libs.thing
import libs.movement_handler

HERO_ANIMATION_JSON_FILE_PATH = "hero_animation_dictionary.json"

HERO_PROPERTIES = {
    'location': [0, 0],
    'size': [60, 111],
    'image_size': [150, 111],
    'json_animation': HERO_ANIMATION_JSON_FILE_PATH,
    'speed': [0, 0],
    'acceleration': [0, 0]
}

platform_list = [
    [0, 200, 200, 50],
    [300, 300, 50, 50],
    [500, 500, 500, 50],
    [0, 800, 2000, 100]
]


class Level:
    def __init__(self, screen):
        self.LEVEL_IS_RUN = True
        # Setup basic screen BG
        self.screen = screen
        self.BG = pygame.image.load("assets/bg.jpg")
        self.BG = pygame.transform.scale(self.BG, (screen.get_width(),
                                                   screen.get_height()))
        # Setup platforms
        self.platform_factory = libs.thing.PlatformFactory()
        for platform_props in platform_list:
            self.platform_factory.create_platform(*platform_props)
        # Creating hero
        self.hero = libs.thing.Hero(**HERO_PROPERTIES)
        # Setup state handler
        self.level_data = {
            "platforms_group": self.platform_factory.get_platform_group().sprites(),
            "hitboxes": self.hero.hitboxes
        }
        self.state_handler = libs.movement_handler.StateHandler(self.level_data, self.hero)

    def process_location(self):
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN or pygame.KEYUP:
                self.handle_keyboard_press(event)
        self.state_handler.handle()
        self.hero.update()

    def handle_keyboard_press(self, key_event):
        if key_event.type == pygame.KEYDOWN:
            self.state_handler.handle_key_press(key_event.key)
            if key_event.key == pygame.K_UP:
                pass
        if key_event.type == pygame.KEYUP:
            self.state_handler.handle_key_release(key_event.key)
            if key_event.key == 27:  # Клавиша ESC
                self.LEVEL_IS_RUN = False

    def display_hero(self):
        self.hero.display_on_screen(self.screen)

    def display_platforms(self):
        self.platform_factory.display_platforms(self.screen)

    def update_screen(self):
        self.screen.blit(self.BG, (0, 0))
        self.display_platforms()
        self.display_hero()
