import pygame
import libs.animationhandler
import libs.movement_handler
import libs.states


class Thing(pygame.sprite.Sprite):
    def __init__(self, **properties):
        pygame.sprite.Sprite.__init__(self)
        # setup animation system
        self.image_size = properties['image_size']
        self.animation_handler = libs.animationhandler.AnimationHandler(properties['json_animation'])
        self.animation_handler.set_image_size(self.image_size)
        # setup basic attributes
        self.image = self.animation_handler.get_current_frame_image()
        self.rect = pygame.Rect(properties['location'], properties['size'])

    def get_sprite_position(self):
        offset = self.animation_handler.get_current_frame_offset()
        x_coord = self.rect.left + offset[0]
        y_coord = self.rect.top + offset[1]
        return [x_coord, y_coord]

    def update(self):
        self.image = self.animation_handler.get_current_frame_image()


class Character(Thing):
    def __init__(self, **properties):
        Thing.__init__(self, **properties)
        self.speed = properties['speed']
        self.acceleration = properties['acceleration']

    def death(self):
        pass


class Hero(Character):
    def __init__(self, **properties):
        Character.__init__(self, **properties)
        self.head = pygame.Rect(self.rect.topleft, (self.rect.width, 1))
        self.legs = pygame.Rect(self.rect.bottomleft, (self.rect.width, 1))
        self.hitboxes = {
            "body": self.rect,
            "head": self.head,
            "legs": self.legs,
        }
        self.movement_handler = libs.movement_handler.MovementHandler(self.hitboxes)
        self.states_dict = libs.states.STATES_DICT

    # MOVEMENTS
    def update(self):  # Update hero location
        self.movement_handler.update_location()
        self.animation_handler.update_current_frame_if_needed()

    def get_hit(self):
        pass

    def display_on_screen(self, screen):
        Thing.update(self)
        # pygame.draw.rect(screen, (0, 0, 100), self.rect)
        screen.blit(self.image, self.get_sprite_position())
        # displaying head and legs
        pygame.draw.rect(screen, (100, 0, 0), self.head)
        pygame.draw.rect(screen, (0, 100, 0), self.legs)


class Platform(Thing):
    def __init__(self, *groups, **properties):
        Thing.__init__(self, **properties)


class PlatformFactory:
    def __init__(self):
        self.platform_group = pygame.sprite.Group()

    def create_platform(self, left, top, width, height):
        properties = {
            'location': [left, top],
            'size': [width, height],
            'image_size': [width, height],
            'json_animation': "platform_animation_dictionary.json",
            'sprite_off': (0, 0),
        }
        new_platform = Platform(**properties)
        new_platform.add(self.platform_group)

    def get_platform_group(self):
        return self.platform_group

    def display_platforms(self, screen):
        self.platform_group.update()
        self.platform_group.draw(screen)
