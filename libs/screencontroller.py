import pygame
import sys
import libs.thing
import libs.level_loader

DEFAULT_HERO_PROPERTIES = {
    'location': [200, 200],
    'size': [150, 111],
    'sprite': pygame.image.load("assets/hero/adventurer-idle-00.png"),
    'sprite_off': (0, 0),
    'is_visible': True,
    'permeable': False,
    'speed': [0, 0],
    'acceleration': [0, 0],
    'max_hp': 3,
    'hp': 3,
    'score': 0,
    'controlled': True,
}


class ScreenController:
    def __init__(self, width, height):
        self.__last_tick = pygame.time.get_ticks()
        self.__current_tick = self.__last_tick
        self.screen = pygame.display.set_mode((width, height))
        self.current_location = "level"  # TODO: should be "menu" on default

    def run(self):
        self.GAME_IS_RUN = True
        while self.GAME_IS_RUN:
            if self.current_location == "menu":
                pass  # run menu
            if self.current_location == "level":
                # RUNNING LEVEL
                level = libs.level_loader.Level(self.screen)
                # Background
                self.LEVEL_IS_RUN = True
                while level.LEVEL_IS_RUN:
                    level.process_location()
                    level.update_screen()
                    pygame.display.update()
            self.GAME_IS_RUN = False
