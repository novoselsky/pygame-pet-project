import pygame
import json


class Frame:
    def __init__(self):
        self.image = None
        self.duration = 0
        self.offset = [0, 0]  # (+x, +y)

    def __set_image(self, image_path):
        self.image = pygame.image.load(image_path)

    def __set_duration(self, duration):
        self.duration = duration

    def __set_offset(self, offset):
        self.offset = offset

    def set_from_dictionary(self, frame_dictionary):
        self.__set_image(frame_dictionary["image_path"])
        self.__set_duration(frame_dictionary["duration"])
        self.__set_offset(frame_dictionary["offset"])

    def get_duration(self):
        return self.duration

    def get_offset(self):
        return self.offset

    def get_image(self):
        image = self.image
        return image


class Animation:
    def __init__(self):
        self.frame_list = []

    def append_frame(self, frame):
        self.frame_list.append(frame)

    def append_frames_from_json_frame_list(self, json_frame_list):
        for frame_dictionary in json_frame_list:
            new_frame = Frame()
            new_frame.set_from_dictionary(frame_dictionary)
            self.append_frame(new_frame)

    def get_frame(self, frame_number):
        return self.frame_list[frame_number]

    def get_frame_amount(self):
        return len(self.frame_list)


class AnimationGroup:
    def __init__(self):
        self.animation_dictionary = {}

    def append_animation(self, name_string, animation):
        self.animation_dictionary[name_string] = animation

    def append_animations_from_json_animation_dictionary(self, group_dictionary):
        for animation in group_dictionary.items():  # (animation_name, json_frame_list)
            new_animation = Animation()
            new_animation.append_frames_from_json_frame_list(animation[1])
            self.append_animation(animation[0], new_animation)

    def append_animations_from_json(self, json_file_name):
        try:
            json_file = open(json_file_name, mode='r')
            json_animation_dictionary = json.load(json_file)
        except Exception:
            raise
        else:
            self.append_animations_from_json_animation_dictionary(json_animation_dictionary)
        finally:
            json_file.close()

    def get_animation(self, animation_name):
        return self.animation_dictionary[animation_name]

    def get_frame(self, animation_name, frame_number):
        frame = self.get_animation(animation_name).get_frame(frame_number)
        return frame

    def get_frame_amount(self, animation_name):
        frame_amount = self.get_animation(animation_name).get_frame_amount()
        return frame_amount


class AnimationHandler:
    def __init__(self, path_to_json_animation_file):
        # setup animation group
        self.animation_group = AnimationGroup()
        self.animation_group.append_animations_from_json(path_to_json_animation_file)
        # setup first frame ("default" animation, zero frame)
        self.current_animation = None
        self.current_frame_number = 0
        self.current_frame = None
        self.__frame_born_tick = pygame.time.get_ticks()
        self.__start_animation("default")
        self.is_flipped = False
        self.image_size = (0, 0)
        self.animation_queue = ["default"]

    def __is_frame_need_to_update(self):
        current_tick = pygame.time.get_ticks()
        current_frame_lifetime = current_tick - self.__frame_born_tick
        current_frame_duration = self.current_frame.get_duration()
        if current_frame_lifetime >= current_frame_duration:
            return True
        else:
            return False

    def __current_animation_is_ended(self):
        current_animation_frame_amount = self.current_animation.get_frame_amount()
        if self.current_frame_number >= current_animation_frame_amount - 1:
            return True
        else:
            return False

    def __update_frame(self):
        self.current_frame_number += 1
        self.current_frame = self.current_animation.get_frame(self.current_frame_number)
        self.__frame_born_tick = pygame.time.get_ticks()

    def update_current_frame_if_needed(self):
        print(f"{pygame.time.get_ticks() - self.__frame_born_tick} "
              f"{self.current_frame_number+1} | {self.current_animation.get_frame_amount()}")
        if self.__is_frame_need_to_update():
            if self.__current_animation_is_ended():
                self.__start_next_animation_if_current_is_ended()
            else:
                self.__update_frame()

    def __get_animation_from_queue_or_none(self):
        new_animation = None
        if self.animation_queue:  # if queue isn't empty
            new_animation = self.animation_queue.pop(0)
        return new_animation

    def __start_current_animation_from_the_beginning(self):
        self.current_frame_number = 0
        self.current_frame = self.current_animation.get_frame(0)
        self.__frame_born_tick = pygame.time.get_ticks()

    def __start_new_animation_from_queue(self):
        new_animation = self.__get_animation_from_queue_or_none()
        if new_animation:
            self.__start_animation(new_animation)
        else:
            self.__start_current_animation_from_the_beginning()

    def __start_next_animation_if_current_is_ended(self):
        if self.__current_animation_is_ended():
            self.__start_new_animation_from_queue()

    def force_animation(self, animation_name):
        self.animation_queue.clear()
        self.__start_animation(animation_name)

    def __start_animation(self, animation_name):
        self.current_animation = self.animation_group.get_animation(animation_name)
        self.current_frame_number = 0
        self.current_frame = self.current_animation.get_frame(0)
        self.__frame_born_tick = pygame.time.get_ticks()

    def add_animation_to_queue(self, animation_name):
        self.animation_queue.append(animation_name)

    def set_flip(self, flip):
        self.is_flipped = flip

    def set_image_size(self, image_size):
        self.image_size = image_size

    def get_current_frame_image(self):
        current_image = self.current_frame.get_image()
        current_image = pygame.transform.flip(current_image, self.is_flipped, 0)
        current_image = pygame.transform.scale(current_image, self.image_size)
        return current_image

    def get_current_frame_offset(self):
        offset = self.current_frame.get_offset()
        return offset
